package com.iteso.pdm18_scrollabletabs;


import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.UserDictionary;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iteso.pdm18_scrollabletabs.beans.ItemProduct;
import com.iteso.pdm18_scrollabletabs.tools.Constant;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTechnology extends Fragment {

    private static final String PROVIDER_NAME = "com.iteso.test.myitesoitems";
    private static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/products/TECHNOLOGY");
    RecyclerView recyclerView;
    ArrayList<ItemProduct> products;
    AdapterProduct adapterProduct;

    public void refreshFragment(){
        ArrayList<ItemProduct> newProducts = new ArrayList<>();


        String[] projection = new String[]{
                CallLog.Calls.TYPE,
                CallLog.Calls.NUMBER};

        Uri llamadasUri = CONTENT_URI;

        ContentResolver cr = getActivity().getContentResolver();

        Cursor cur = cr.query(llamadasUri,
                projection, //Columnas a devolver
                null,       //Condición de la query
                null,       //Argumentos variables de la query
                null);      //Orden de los resultados

        if (cur.moveToFirst()) {
            String nameOfProduct = "";
            newProducts = new ArrayList<>();
            do {
                nameOfProduct = cur.getString(1);
                newProducts.add(new ItemProduct(6, nameOfProduct, ""));

            } while (cur.moveToNext());
        }

        AdapterProduct adapterProduct = new AdapterProduct(Constant.FRAGMENT_TECHNOLOGY, getActivity(), newProducts);
        recyclerView.swapAdapter(adapterProduct, true);
    }

    public FragmentTechnology() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_technology, container, false);
        recyclerView = rootView.findViewById(R.id.fragment_recycler);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String[] projection = new String[]{
                CallLog.Calls.TYPE,
                CallLog.Calls.NUMBER};

        Uri llamadasUri = CONTENT_URI;

        ContentResolver cr = getActivity().getContentResolver();

        Cursor cur = cr.query(llamadasUri,
                projection, //Columnas a devolver
                null,       //Condición de la query
                null,       //Argumentos variables de la query
                null);      //Orden de los resultados

        if (cur.moveToFirst()) {
            String nameOfProduct = "";
            products = new ArrayList<>();
            do {
                nameOfProduct = cur.getString(1);
                products.add(new ItemProduct(6, nameOfProduct, ""));

            } while (cur.moveToNext());
        }

        recyclerView.setHasFixedSize(true);
        // Use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        adapterProduct = new AdapterProduct(Constant.FRAGMENT_TECHNOLOGY, getActivity(), products);
        recyclerView.setAdapter(adapterProduct);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ItemProduct itemProduct = data.getParcelableExtra(Constant.EXTRA_PRODUCT);
        Iterator<ItemProduct> iterator = products.iterator();
        int position = 0;
        while (iterator.hasNext()) {
            ItemProduct item = iterator.next();
            if (item.getCode() == itemProduct.getCode()) {
                products.set(position, itemProduct);
                break;
            }
            position++;
        }
        adapterProduct.notifyDataSetChanged();

    }
}
