package com.iteso.pdm18_scrollabletabs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iteso.pdm18_scrollabletabs.beans.ItemProduct;
import com.iteso.pdm18_scrollabletabs.tools.Constant;

import java.util.ArrayList;

/**
 * @author Oscar Vargas
 * @since 26/02/18.
 */

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.ViewHolder> {

    private ArrayList<ItemProduct> products;
    private Context context;
    private int fragment;

    AdapterProduct(int fragment, Context context, ArrayList<ItemProduct> products) {
        this.fragment = fragment;
        this.products = products;
        this.context = context;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mStore;
        TextView mLocation;
        TextView mPhone;
        RelativeLayout mDetail;
        ImageView mImage;

        ViewHolder(View v) {
            super(v);
            mTitle = v.findViewById(R.id.item_product_title);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mTitle.setText(products.get(holder.getAdapterPosition()).getTitle());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
